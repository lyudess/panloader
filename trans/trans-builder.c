/*
 * © Copyright 2018 The Panfrost Community
 *
 * This program is free software and is provided to you under the terms of the
 * GNU General Public License version 2 as published by the Free Software
 * Foundation, and any use by you of this program is subject to the terms
 * of such GNU licence.
 *
 * A copy of the licence is included with the program, and can also be obtained
 * from Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 */

#include "trans-builder.h"

/* The trans builder, as used in the _trans_itory driver (hence the name,
 * obviously!), exposes a thin layer over the hardware. It exposes a low-level
 * API for building up the GPU memory data structures based on state. It
 * explicitly does _not_ attempt to model OpenGL or other high-level APIs. */

/* Framebuffer descriptor */

/* Set dimensions of the framebuffer */

void
trans_framebuffer_dimensions(struct mali_single_framebuffer *sfbd, int width, int height)
{
	sfbd->width = MALI_POSITIVE(width);
	sfbd->height = MALI_POSITIVE(height);
}

/* Helpers used by the below functions */

/* Maps float 0.0-1.0 to int 0x00-0xFF */
uint8_t normalised_float_to_u8(float f) {
	return (uint8_t) (int) (f * 255.0f);
}

void
trans_framebuffer_clear(
		struct mali_single_framebuffer *sfbd,
		mali_ptr depth_stencil_buffer, 
		bool clear_color, bool clear_depth, bool clear_stencil, 
		float r, float g, float b, float a, float depth, u32 stencil)
{
	uint32_t packed_color =
		(normalised_float_to_u8(a) << 24) |
		(normalised_float_to_u8(b) << 16) |
		(normalised_float_to_u8(g) <<  8) |
		(normalised_float_to_u8(r) <<  0);

	/* Fields duplicated 4x for unknown reasons. Same in Utgard, too, which
	 * is doubly weird. */

	if (clear_color) {
		sfbd->clear_color_1 = packed_color;
		sfbd->clear_color_2 = packed_color;
		sfbd->clear_color_3 = packed_color;
		sfbd->clear_color_4 = packed_color;
	}

	if (clear_depth) {
		sfbd->depth_buffer = depth_stencil_buffer;
		sfbd->depth_buffer_enable = MALI_DEPTH_STENCIL_ENABLE;

		sfbd->clear_depth_1 = depth;
		sfbd->clear_depth_2 = depth;
		sfbd->clear_depth_3 = depth;
		sfbd->clear_depth_4 = depth;
	}

	if (clear_stencil) {
		sfbd->depth_buffer = depth_stencil_buffer;
		sfbd->depth_buffer_enable = MALI_DEPTH_STENCIL_ENABLE;

		sfbd->clear_stencil = stencil;
	}

	/* XXX: What do these flags mean? */
	sfbd->clear_flags = 0x00101100;

	/* Fast path? */
	if (clear_color && clear_depth && clear_stencil) {
		sfbd->clear_flags |= MALI_CLEAR_FAST;
	} else {
		sfbd->clear_flags |= MALI_CLEAR_SLOW;

		if (clear_stencil)
			sfbd->clear_flags |= MALI_CLEAR_SLOW_STENCIL;
	}
}

/* nullForVertex */

struct mali_viewport
trans_viewport(int viewport_x0, int viewport_y0,
		  int viewport_x1, int viewport_y1)
{
	/* Viewport encoding is asymmetric. Purpose of the floats is unknown? */

	struct mali_viewport ret = {
		.floats = {
			-inff, -inff,
			inff, inff,
			0.000000f, 1.000000f,
		},
		.viewport0 = { viewport_x0, viewport_y0 },
		.viewport1 = { MALI_POSITIVE(viewport_x1), MALI_POSITIVE(viewport_y1) },
	};

	return ret;
}

/* Shared descriptor for attributes as well as varyings, which can be
 * considered like a matrix */

struct mali_attr
trans_attr(mali_ptr data, size_t type_sz, int columns, int vertices)
{
	int stride = type_sz * columns;

	struct mali_attr ret = {
		.elements = data | 1,
		.stride = stride,
		.size = stride * vertices
	};

	return ret;
}

/* Vertex/tiler payload */

void
trans_rasterizer_state(struct mali_payload_vertex_tiler *vt,
		float line_width,
		int front_face)
{
	vt->line_width = line_width;
	vt->gl_enables |= MALI_GL_FRONT_FACE(front_face);
}

/* Fragment payload */

struct mali_payload_fragment
trans_fragment_payload(int width, int height, mali_ptr fbd, enum mali_fbd_type type)
{
	struct mali_payload_fragment ret = {
		.min_tile_coord = MALI_COORDINATE_TO_TILE_MIN(0, 0, 0),
		.max_tile_coord = MALI_COORDINATE_TO_TILE_MAX(width, height, 0),
		.framebuffer = fbd | type,
	};

	return ret;
}
