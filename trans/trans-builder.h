/*
 * © Copyright 2018 The Panfrost Community
 *
 * This program is free software and is provided to you under the terms of the
 * GNU General Public License version 2 as published by the Free Software
 * Foundation, and any use by you of this program is subject to the terms
 * of such GNU licence.
 *
 * A copy of the licence is included with the program, and can also be obtained
 * from Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 */

#ifndef __BUILDER_H__
#define __BUILDER_H__

#include "pandev.h"

void trans_framebuffer_dimensions(struct mali_single_framebuffer *sfbd, int width, int height);

void trans_framebuffer_clear(
		struct mali_single_framebuffer *sfbd,
		mali_ptr depth_stencil_buffer, 
		bool clear_color, bool clear_depth, bool clear_stencil, 
		float r, float g, float b, float a, float depth, u32 stencil);

struct mali_viewport
trans_viewport(int viewport_x0, int viewport_y0,
		int viewport_x1, int viewport_y1);

void
trans_rasterizer_state(struct mali_payload_vertex_tiler *vt,
		float line_width,
		int front_face);

struct mali_attr trans_attr(mali_ptr data, size_t type_sz, int columns, int vertices);

struct mali_payload_fragment trans_fragment_payload(int width, int height, mali_ptr fbd, enum mali_fbd_type type);


#endif
